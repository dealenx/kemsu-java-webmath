function onFileLoad(elementId, event) {
    document.getElementById(elementId).innerText = event.target.result;
    
    var file_cont = {
        data: event.target.result 
    }
    console.log(JSON.stringify(file_cont));
    document.getElementById("file_content").value = JSON.stringify(file_cont); 
}

function onChooseFile(event, onLoadFileHandler) {
    if (typeof window.FileReader !== 'function')
        throw ("The file API isn't supported on this browser.");
    let input = event.target;
    if (!input)
        throw ("The browser does not properly implement the event object");
    if (!input.files)
        throw ("This browser does not support the `files` property of the file input.");
    if (!input.files[0])
        return undefined;
    let file = input.files[0];
    let fr = new FileReader();
    fr.onload = onLoadFileHandler;
    fr.readAsText(file);
}

