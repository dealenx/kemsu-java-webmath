<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=Windows-1251" pageEncoding="Windows-1251" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=Windows-1251">
  <title>Index</title>
  
  <style type="text/css">
    <%@include file="/WEB-INF/css/style.css" %>
    </style>

 </head> 
<%
        
        ArrayList<String[]> tables = (ArrayList<String[]>)request.getAttribute("tables");
        String uname = (String)request.getAttribute("uname");    
     %>    
      
 <body>
     
     <div class="site-nav"> 
        <p> Hello, <%=""+uname%>! </p>
        <a href="${pageContext.request.contextPath}/logout">Exit </a>   
     </div>
        
     <table>
    <thead>
        <tr>
            <th>id</th>  
            <th>name</th> 
            <th>n</th>
            <th>iterations</th>
            <th>action</th>
        </tr>
        
    </thead>
  <tbody>
 
    
    <% for(int i = 0; i < tables.size(); i++) { %>
            <tr>      
                <td><%=""+tables.get(i)[0]%></td>  
                <td><%=""+tables.get(i)[1]%></td>
                <td><%=""+tables.get(i)[2]%></td>
                <td><%=""+tables.get(i)[3]%></td>
                <td class="actions-area">
                    <form method="post" action="${pageContext.request.contextPath}/home/delete">   
                        <input type="text" name="id" value="<%=""+tables.get(i)[0]%>" style="display: none;"> 
                       <input type="submit" value="delete">
                    </form>
                    <form method="post" action="${pageContext.request.contextPath}/home/upload">   
                        <input type="text" name="id" value="<%=""+tables.get(i)[0]%>" style="display: none;"> 
                        <input type="text" name="name" value="<%=""+tables.get(i)[1]%>" style="display: none;"> 
                       <input type="submit" value="download"> 
                    </form>
                </td>
                
            </tr>
        <% } %>
    
  </tbody>
  </table> 
  <form method="post" action="${pageContext.request.contextPath}/home/load" class="site-form">   
      <input type="text" name="textNAME" value="name"> 
     <input type="text" name="textN" value="20"> 
     <input type="text" name="textK" value="2"> 
     <input type="text" name="textEPS" value="0.001"> 
     <input type="submit" value="solve">
  </form>
      
   <p id="contents"></p>
 
   
   <script type="text/javascript">
        <%@include file="/WEB-INF/js/main.js" %> 
    </script>
     
 </body>

</html>