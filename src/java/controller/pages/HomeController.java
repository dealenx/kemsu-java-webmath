/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.pages;

import model.Auth;
import model.Model;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/home")
public class HomeController extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Model model = new Model(request, response);

        if (model.auth.isAllowed()) {

            request.setAttribute("uname", model.auth.getUserName());
            request.setAttribute("tables", model.getData(model.auth.getUserName()));

            request.getRequestDispatcher("/WEB-INF/views/home.jsp").forward(request, response);
        } else {
            response.sendRedirect("http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/");
        }
    }
}
