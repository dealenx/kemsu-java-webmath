package controller.request;

import model.Auth;
import java.io.PrintWriter;

import model.Model;

import java.io.File;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import javax.servlet.ServletException;
import java.io.IOException;
import javax.servlet.http.Part;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

@WebServlet("/home/delete")
public class DeleteController extends HttpServlet {
    
    Model m = new Model();

    public static boolean deleteFile(String filePath) {

        File file = new File(filePath);

        return file.delete();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Model model = new Model(request, response);
        PrintWriter printWriter = null;
        try {
            if (model.auth.isAllowed()) {
                int id = Integer.parseInt(request.getParameter("id"));
                String id_str = request.getParameter("id");
                   
                m.deleteRowFromDataBase(id, model.auth.getUserName());

                response.sendRedirect("http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/home");

            } else {
                response.sendRedirect("http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/");
            }

        } catch (Exception ex) {
            printWriter.println("Error: " + ex.getMessage());
        }
    }
}
