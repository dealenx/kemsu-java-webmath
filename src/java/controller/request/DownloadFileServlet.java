/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.request;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Auth;
import model.Model;

@WebServlet("/home/upload")
public class DownloadFileServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,HttpServletResponse response) 
    throws ServletException, IOException {
        Model model = new Model(request, response);
        if (model.auth.isAllowed()) {
            String id = request.getParameter("id");
            String name = request.getParameter("name");
            // reads input file from an absolute path
            

            // obtains ServletContext
            ServletContext context = getServletContext();

            // gets MIME type of the file
            String mimeType = "application/octet-stream";
   
            System.out.println("MIME type: " + mimeType);
            
            String temp_get =  model.getDataResult(model.auth.getUserName(), Integer.parseInt(id));
            System.out.println("temp_get : " + temp_get );
            //temp_get = "123";
            byte[] buffer_temp = temp_get.getBytes();

            // modifies response
            response.setContentType(mimeType);
            //response.setContentLength((int)  String.valueOf(temp_get));

            // forces download
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", name + ".dat");
            response.setHeader(headerKey, headerValue);
            
            

            // obtains response's output stream
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            outStream.write(buffer_temp);

          
            outStream.close();
        } else {
            response.sendRedirect("http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/");
        }

    }
}
