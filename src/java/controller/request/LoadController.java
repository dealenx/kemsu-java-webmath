package controller.request;

import helmgoltz.HResult;
import model.Auth;
import java.io.PrintWriter;

import model.Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import javax.servlet.ServletException;
import java.io.IOException;
import javax.servlet.http.Part;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

@WebServlet("/home/load")
public class LoadController extends HttpServlet {

    public void createFile(String name) {
        try (FileWriter fw = new FileWriter(name)) {
            fw.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Model model = new Model(request, response);
        
        PrintWriter printWriter = null;
        try {
            if (model.auth.isAllowed()) {
                int n = Integer.parseInt(request.getParameter("textN"));
                int k = Integer.parseInt(request.getParameter("textK"));
                double eps = Double.parseDouble(request.getParameter("textEPS"));
                String nameSolve = request.getParameter("textNAME");

                
                model.startSolve(nameSolve, n, k, eps, model.auth.getUserName());
                HResult hr = model.getHResult();
                Integer last_id = new Integer(model.GetLastId(model.auth.getUserName()));

                String path_file = "/home/dealenx/dev/WebMath/users/" + model.auth.getUserName() + "/" + last_id.toString() + ".dat";
                createFile(path_file);
                hr.saveFileResult(path_file);
                //writeInfoFile(path_file);
                response.sendRedirect("http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/home");

            } else { 
                response.sendRedirect("http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/");
            }

        } catch (Exception ex) {
            printWriter.println("Error: " + ex.getMessage());
        }
    }
}
