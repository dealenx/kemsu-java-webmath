package database;

import java.sql.*;
import java.util.ArrayList;

public class DataBase {

    private String url, login, password;
    private Connection con;
    private Statement statement;
    private ResultSet rs;

    public boolean errorDataBase;
    public String infoError;
    private int countRows;

    public DataBase() { 
        init();
        connect();
    }

    private void init() {
        url = "jdbc:postgresql://localhost:5432/test";
        login = "postgres";
        password = "123";
        errorDataBase = false;
        countRows = 0;
    }
    
    private void connect() {
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(url, login, password);

            try {
                statement = con.createStatement();
            } catch (Exception e) {
                errorDataBase = true;
                infoError = "Error create statment: " + e;
            }

        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error connect to data base: " + e;
        }

    }

    private void disConnect() {
        try {
            rs.close();

            try {
                statement.close();

                try {
                    con.close();
                } catch (Exception e) {
                    errorDataBase = true;
                    infoError = "Error disconnect data base: " + e;
                }

            } catch (Exception e) {
                errorDataBase = true;
                infoError = "Error statement close: " + e;
            }

        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error close tables: " + e;
        }
    }
    
    /*AUTH*/
    
    public void updateSessionId(String username, String sessionid) {         
        try { 
         con.setAutoCommit(false);

         statement = con.createStatement();
         String sql = "UPDATE \"users\" " +  
                   "SET sessionid =  '" + sessionid + "' " + 
                   "WHERE name = '" + username + "' ;";
         statement.executeUpdate(sql);

         statement.close();
         con.commit(); 

        } catch (Exception e) {
            errorDataBase = true;
            System.out.println("Error write data base: " + e);
            infoError = "Error write data base: " + e;
        }
    }
    
    public String getSessionID(String name) {
        String temp = "";
        try {
            String sql = "SELECT sessionid FROM \"users\" WHERE name = \'" + name + "\';  ";
            rs = statement.executeQuery(sql);
            rs.next();
            temp = rs.getString("sessionid");

        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error get count rows: " + e;
        }
        
        System.out.println("tmp: " + temp);
        return temp;
    }
    
    /* CALC DATA */

    public void sendData(String name, int n, int iter, String user, String data_result) {
        int last_id = getLastID(user) + 1;
        
        try {
            countRows = getCountRows();
            System.out.println("a  = " + countRows);

            String data = "INSERT INTO \"calc\" (id, name, n, iterations, usern, result) "
                    + "VALUES (" + last_id + ", \'" + name + "\', " + n + ", " + iter + ", \'" + user + "\', \'" + data_result + "\');";
            statement.executeUpdate(data);

        } catch (Exception e) {
            errorDataBase = true;
            System.out.println("Error write data base: " + e);
            infoError = "Error write data base: " + e;
        }
        
        System.out.println("error = " + errorDataBase);
    }

    private int getCountRows() {
        int tmp = 0;
        try {
            rs = statement.executeQuery("SELECT count(id) FROM \"calc\"");
            rs.next();
            tmp = rs.getInt(1);
        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error get count rows: " + e;
        }
        return tmp;
    }
    
    public String getDataResult(String username, int id) {
        
        String tmp = "";
        try {
            rs = statement.executeQuery("SELECT result FROM \"calc\" WHERE usern = \'" + username + "\' and id = " + id + "  ;  ");
            rs.next();
            tmp = rs.getString("result");

        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error get count rows: " + e;
        }
        
        System.out.println("tmp: " + tmp);
        return tmp;
    }

    public int getLastID(String username) {
        
        int tmp = 0;
        try {
            rs = statement.executeQuery("SELECT id FROM \"calc\" WHERE usern = \'" + username + "\'  ORDER BY id DESC LIMIT 1;  ");
            rs.next();
            tmp = rs.getInt("id");

        } catch (Exception e) {
            errorDataBase = true;
            infoError = "Error get count rows: " + e;
        }
        
        System.out.println("tmp: " + tmp);
        return tmp;
    }

    private String deleteRequest(int id, String username) {
        return "DELETE FROM \"calc\" where id = " + id + " and usern = \'" + username + "\';";
    }

    public void deleteRow(int id, String username) {
        
        try {
            String data = deleteRequest(id, username);
            statement.executeUpdate(data);

        } catch (Exception e) {
            errorDataBase = true;
        }
        

    }

    

    public ArrayList<String[]> getData(String user) {
        
        ArrayList<String[]> list = new ArrayList<String[]>();
        try {
            rs = statement.executeQuery("SELECT * FROM \"calc\" WHERE usern = '" + user + "'");
            while (rs.next()) {
                String[] ls = {rs.getString("id"), rs.getString("name"), rs.getString("n"), rs.getString("iterations")};
                list.add(ls);
            }
        } catch (Exception e) {
            System.out.println("e = " + e);
        }
        
        return list;

    }
    
    
    
    

}
