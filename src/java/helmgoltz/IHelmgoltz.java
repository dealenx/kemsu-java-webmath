/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helmgoltz;

/**
 *
 * @author 1
 */
public interface IHelmgoltz {

    public void setData(int n, double eps, double k);

    public void solve();

    public HResult getResult();

    public void setHole(int numberBorder, double y1, double y2, double speed, int flag);
}
