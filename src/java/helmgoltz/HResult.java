/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helmgoltz;

import java.io.FileWriter;
import java.io.IOException;

public class HResult {

    public String name;

    public int iterations;
    public double eps;
    public double h;
    public int n;

    public double[][] psi;
    public double[][] u;
    public double[][] v;
    public double[] x;
    public double[] y;

    public void printResult() {
        System.out.println("name = " + name);
        System.out.println("eps = " + eps);
        System.out.println("h = " + h);
        System.out.println("n = " + n);
        System.out.println("iterations = " + iterations);
        System.out.println("");
    }

    public void saveFileResult(String path) {
        FileWriter writer;
        try {
            writer = new FileWriter(path, false);
            writer.write("TITLE=\"" + name + "\"\n");
            writer.write("VARIABLES=x,y,psi,u,v\n");
            writer.write("ZONE T=TEST, I=" + (n + 1) + ", J=" + (n + 1) + ",F=POINT\n");
            for (int i = 0; i < n + 1; i++) {
                for (int j = 0; j < n + 1; j++) {
                    writer.write(x[j] + " " + y[i] + " " + psi[i][j] + " " + u[i][j] + " " + -v[i][j] + "\n");
                }
            }
            writer.close();
        } catch (IOException ex) {
            System.out.println("error " + ex);
        }
    }

    public String getDataResult() {
        String temp_return = "";
        temp_return += "TITLE=\"" + name + "\"\n";
        temp_return += "VARIABLES=x,y,psi,u,v\n";
        temp_return += "ZONE T=TEST, I=" + (n + 1) + ", J=" + (n + 1) + ",F=POINT\n";
        for (int i = 0; i < n + 1; i++) {
            for (int j = 0; j < n + 1; j++) {
                temp_return += x[j] + " " + y[i] + " " + psi[i][j] + " " + u[i][j] + " " + -v[i][j] + "\n";
            }
        }

        return temp_return;
    }
}
