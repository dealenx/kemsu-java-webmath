package helmgoltz;

public class Hole {

    int number;
    double y0;
    double y1;
    double u;
    int flag;

    public Hole(int n, double yy0, double yy1, double uu, int flagg) {
        number = n;
        y0 = yy0;
        y1 = yy1;
        u = uu;
        flag = flagg;
    }
}
