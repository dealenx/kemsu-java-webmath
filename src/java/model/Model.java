package model;

import database.DataBase;
import java.util.ArrayList;
import solver.*;
import helmgoltz.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Model {

    public DataBase idB;
    public IPoisson ip;
    public IHelmgoltz ih;
    public Auth auth;

    public Model() {
        init();
    }
    public Model(HttpServletRequest request, HttpServletResponse response) {
        init();
        auth = new Auth(request, response, idB);
    }
    

    private void init() {
        idB = new DataBase();
        ip = new Poisson();
    }

    public int GetLastId(String username) {
        int temp = idB.getLastID(username);
        return temp;
    }
    public String getDataResult(String username, int id) {
        return idB.getDataResult(username, id);
    }
    public void startSolve(String name, int n, int k, double eps, String user) {
        ih = new Helmgoltz(name);
        ih.setData(n, eps, k);
        ih.setHole(1, 0.4, 0.6, 1, 0);
        ih.setHole(3, 0.4, 0.6, 1, 1);
        try {
            ih.solve();
            HResult hr = ih.getResult();
            //hr.saveFileResult("C:/Users/1/Desktop/DataSolome/");
            idB.sendData(hr.name, hr.n, hr.iterations, user, hr.getDataResult());

        } catch (NullPointerException ex) {
            System.out.println("error: " + ex);
        }
    }

    public HResult getHResult() {
        return ih.getResult();
    }

    public void deleteRowFromDataBase(int id, String username) {
        idB.deleteRow(id, username);
    }

    public void sendDataToDataBase(int a, int b, int n, double eps, double err, int iter) {
        //idB.sendData(a, b, n, eps, err, iter);
    }

    public ArrayList<String[]> getData(String user) {
        return idB.getData(user);
    }

    public boolean getErrorDataBase() {
        return false;//idB.errorDataBase;//!!!!!!!!!!!!!!!!!!!
    }

    public double startPoisson(int n) {
        ip.setData(0, 1, n, 0.0001);
        ip.solve();
        double r = ip.getError();
        int iter = ip.getIterations();
        sendDataToDataBase(0, 1, n, 0.0001, r, iter);
        return r;
    }
}
