/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import database.DataBase;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Auth {

    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Cookie[] cookies;

    private String username;
    private String password;
    private String sessionID;
    private DataBase db;

    public Auth(HttpServletRequest request, HttpServletResponse response, DataBase db) {
        this.request = request;
        this.response = response;
        this.cookies = request.getCookies();
        this.db = db;
        
        setUserName();
        setPassWord();
        setSessionID();

    }

    private void writeIntoCookie(String name, String value) {
        String temp = value;
        this.response.addCookie(new Cookie(name, temp));
    }

    private String getFromCookie(String name) {
        String temp_return = "";
        if (this.cookies != null) {
            for (Cookie cookie : this.cookies) {
                if (cookie.getName().equals(name)) {
                    temp_return = cookie.getValue();
                }
            }
        }
        return temp_return;
    }
    
    private void clearCookie(String name) { 
        Cookie temp_cookie = new Cookie(name, ""); 
        temp_cookie.setMaxAge(0);
        this.response.addCookie(temp_cookie); 
    }

    public void logout() {
        clearCookie("username");
        clearCookie("password"); 
        clearCookie("sessionID"); 
        
        this.db.updateSessionId(getUserName(), "" );
    }

    public void login() {
        writeIntoCookie("username", this.request.getParameter("username"));
        writeIntoCookie("password", this.request.getParameter("password"));
        writeIntoCookie("sessionID", this.request.getSession().getId() ); 
        
        setUserName();
        setPassWord();
        setSessionID();
        
        this.db.updateSessionId(this.request.getParameter("username"), this.request.getSession().getId() );
        
    }

    private void setUserName() {
        this.username = getFromCookie("username");
    }

    private void setPassWord() {
        this.password = getFromCookie("password");
    }

    private void setSessionID() {
        this.sessionID = getFromCookie("sessionID");
    }

    public String getUserName() {
        return this.username;
    }

    public String getPassWord() {
        return this.password;
    }

    public String getSessionID() {
        return this.sessionID;
    }

    public boolean isAllowed() {
        System.out.println("getSessionID(): " + getSessionID()); 
        System.out.println("db.getSessionID(uname): " + db.getSessionID( getUserName() ));  
        String temp_sessionid = getSessionID(); 
        String temp_db_SessionID = db.getSessionID( getUserName() );
        
        boolean status = false;
        if (!temp_db_SessionID.contentEquals("")) {  
            if (temp_sessionid.contentEquals(temp_db_SessionID)) { 
                return status = true;
            }
        }
        return status;
    }
}
