/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solver;

/**
 *
 * @author 1
 */
public interface IPoisson {
    
    public void setData(double a, double b, int n, double eps);
    
    public void solve();
    
    public double[][] getResult();
    public double getError();
    public int getIterations();

}
