package solver;


import static java.lang.Math.*;
//import IEnterData;

public class Poisson implements IPoisson{
    
    double eps;

    double[][] U;
    double[][] Uk;
    double[] x;
    double[] y;
    
    double[][] r;
    double[][] f;
    double[][] analitic;
    double[][] error;

    int n;
    double h;
    double a, b;
    double k1, k2;
    double tau;
    int iterations;
    double norm;
    double e;  
    
    public Poisson()
    {
        
    }
    
    @Override
    public void solve()
    {
        init();
	fillVectors();
	calcAnalitic();
	calc();
	calcError();
    }
    
    private void init()
    {
	k1 = k2 = 1;
	norm = 0;
	x = new double[n];
	y = new double[n];
	r = new double[n][n];
	U = new double[n][n];
	Uk = new double[n][n];
	f = new double[n][n];
	error = new double[n][n];
	analitic = new double[n][n];
        
        h = (b - a) / (n - 1);
	tau = 0;
    }
    
    private void fillVectors()
    {
        for (int i = 0; i < n; i++)
	{
            x[i] = a + i * h;
            y[i] = a + i * h;
	}

	for (int i = 0; i < n; i++)
	{
            for (int j = 0; j < n; j++)
            {
		f[i][j] = ((k1*k1) + (k2*k2)) * PI*PI*sin(x[i] * PI*k1) * sin(y[j] * PI*k2);
            }
	}
    }
    
    private boolean conditions(double norm)
    {
	return calcError1() < eps;
    }
    private double calcError1()
    {
	double max = 0;

	for (int i = 0; i < n; i++)
	{
            for (int j = 0; j < n; j++)
            {
		error[i][j] = abs(Uk[i][j] - U[i][j]);
            }
	}

	for (int i = 0; i < n; i++)
	{
            for (int j = 0; j < n; j++)
            {
		if (error[i][j] > max)
        	{
                    max = error[i][j];
		}
            }
	}
	return max;
    }
    
    private void findR()
    {
	for (int i = 1; i < n-1; i++)
	{
            for (int j = 1; j < n-1; j++)
            {
		r[i][j] = -((Uk[i + 1][j] - 2 * Uk[i][j] + Uk[i-1][j]) / pow(h,2)) -
                    ((Uk[i][j+1] - 2 * Uk[i][j] + Uk[i][j-1]) / pow(h, 2)) - f[i][j];
            }
	}
    }
    
    public void calc()
    {
	while (true)
	{
            norm = 0;
            iterations++;
            findR();
            tau = Arn_rn() / Arn_Arn();
            for (int i = 1; i < n - 1; i++)
            {
                for (int j = 1; j < n - 1; j++)
		{
                    U[i][j] = Uk[i][j] - tau * r[i][j];
		}
            }

            for (int i = 1; i < n-1; i++)
            {
                for (int j = 1; j < n-1; j++)
		{
                    norm += r[i][j] * r[i][j];
		}
            }
            
            if (calcError1() < eps)
            {
                break;
            }
            
            for (int i = 0; i < n; i++)
            {
                System.arraycopy(U[i], 0, Uk[i], 0, n);
            }
        }
    }
    private void calcAnalitic()
    {	
	for (int i = 0; i < n; i++)
	{
            for (int j = 0; j < n; j++)
            {
		analitic[i][j] = sin(x[i] * PI*k1) * sin(y[j] * PI*k2);
            }
	}
    }
    
    private double Arn_rn()
    {
	double c = 0;

	for (int i = 1; i < n - 1; i++)
	{
            for (int j = 1; j < n - 1; j++)
            {
		c += (-(r[i + 1][j] - 2 * r[i][j] + r[i - 1][j]) / pow(h, 2) -
                    (r[i][j + 1] - 2 * r[i][j] + r[i][j - 1]) / pow(h, 2)) * r[i][j];
            }
	}
        c *= (h*h); 
	return c;
    }
    
    private double Arn_Arn()
    {
    double c = 0;

    for (int i = 1; i < n - 1; i++)
    {
        for (int j = 1; j < n - 1; j++)
	{
            c += pow((-(r[(i + 1)][j] - 2 * r[i][j] + r[i - 1][j]) / (h*h) -
                (r[i][j + 1] - 2 * r[i][j] + r[i][j - 1]) / (h*h)), 2);
	}
    }
    c *= (h*h);
    return c;
    }
    
    private double calcError()
    {
	double max = 0;

	for (int i = 0; i < n; i++)
	{
            for (int j = 0; j < n; j++)
            {
		error[i][j] = abs(analitic[i][j] - U[i][j]);
            }
	}

	for (int i = 0; i < n; i++)
	{
            for (int j = 0; j < n; j++)
            {
		if (error[i][j] > max)
		{
                    max = error[i][j];
		}
            }
	}
        return max;
    }

    @Override
    public void setData(double a, double b, int n, double eps) {
        this.a = a;
        this.b = b;
        this.n = n;
        this.eps = eps;
    }

    @Override
    public double[][] getResult() {
        return U;
    }

    @Override
    public double getError() {
        return calcError();
    }

    @Override
    public int getIterations() {
        return iterations;
    }

}
